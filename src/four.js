"use strict";
const { readFileIntoArray } = require("./util/fs");

/**
 * Read passports file
 * @param path
 * @return {[*[], number]}
 */
function readPassports(path = "input/four.txt") {
  return readFileIntoArray(path).reduce(
    ([lines, index], line) => {
      if (line.length > 0) {
        if (!lines[index]) lines[index] = line;
        else lines[index] += " " + line;
      } else {
        ++index;
      }
      return [lines, index];
    },
    [[], 0]
  );
}

function isCompletePassport(attributes) {
  // Valid passport
  if (attributes.length === 8) {
    return true;
  }

  // Could be valid if cid is missing
  if (attributes.length === 7) {
    const cid = attributes.find(
      (attribute) => attribute.indexOf("cid:") !== -1
    );
    if (!cid) {
      return true;
    }
  }

  return false;
}

const MANDATORY_ATTRIBUTES = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];

/**
 * Check if passport attributes are valid
 * @param attributes
 * @return {boolean}
 */
function isValidPassport(attributes) {
  // not valid because of not enough attributes
  if (attributes.length <= 6) {
    return false;
  }

  // Valid passport
  // byr (Birth Year) - four digits; at least 1920 and at most 2002.
  // iyr (Issue Year) - four digits; at least 2010 and at most 2020.
  // eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
  // hgt (Height) - a number followed by either cm or in:
  //   If cm, the number must be at least 150 and at most 193.
  //   If in, the number must be at least 59 and at most 76.
  // hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
  // ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
  // pid (Passport ID) - a nine-digit number, including leading zeroes.
  // cid (Country ID) - ignored, missing or not.
  let duplicates = [];
  for (const attribute of attributes) {
    const [key, value] = attribute.split(":");
    if (!duplicates.includes(key)) {
      duplicates.push(key);
    }
    switch (key) {
      case "byr":
        if (
          value.length !== 4 ||
          parseInt(value) < 1920 ||
          parseInt(value) > 2002
        ) {
          return false;
        }
        break;
      case "iyr":
        if (
          value.length !== 4 ||
          parseInt(value) < 2010 ||
          parseInt(value) > 2020
        ) {
          return false;
        }
        break;
      case "eyr":
        if (
          value.length !== 4 ||
          parseInt(value) < 2020 ||
          parseInt(value) > 2030
        ) {
          return false;
        }
        break;
      case "hgt":
        if (value.indexOf("cm") > -1) {
          const heightCm = parseInt(value.substr(0, value.length - 2));
          if (heightCm < 150 || heightCm > 193) {
            return false;
          }
        } else if (value.indexOf("in") > -1) {
          const heightIn = parseInt(value.substr(0, value.length - 2));
          if (heightIn < 59 || heightIn > 76) {
            return false;
          }
        } else {
          return false;
        }
        break;
      case "hcl":
        const hcl = /^#[0-9a-f]+/;
        if (value.length !== 7 || !hcl.test(value)) {
          return false;
        }
        break;
      case "ecl":
        if (
          !["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].includes(value)
        ) {
          return false;
        }
        break;
      case "pid":
        const pid = /[0-9]+/;
        if (value.length !== 9 || !pid.test(value)) {
          return false;
        }
        break;
    }
  }

  for (const mandatory of MANDATORY_ATTRIBUTES) {
    if (!duplicates.includes(mandatory)) {
      return false;
    }
  }
  return true;
}

module.exports = {
  readPassports,
  isValidPassport,
  isCompletePassport,
};
