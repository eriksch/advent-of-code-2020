const lcm = require("compute-lcm");

/**
 * Wait for the bus
 * @param timestamp
 * @param list
 * @return {number}
 */
function wait(timestamp, list) {
  const arrivalAtShuttle = parseInt(timestamp);
  const shuttles = list
    .split(",")
    .filter((shuttle) => shuttle !== "x")
    .map((shuttle) => parseInt(shuttle));
  const schedule = new Map();
  shuttles.forEach((shuttle) => {
    let i = 0,
      departure = 0;
    while (departure < arrivalAtShuttle) {
      departure = shuttle * i++;
    }
    if (schedule.has(departure)) {
      schedule.set(departure, [shuttle, ...schedule.get(departure)]);
    } else {
      schedule.set(departure, [shuttle]);
    }
  });
  const sortedOnTime = Array.from(schedule.keys()).sort((a, b) => a - b);
  const firstToDepart = sortedOnTime[0];
  return (firstToDepart - arrivalAtShuttle) * schedule.get(firstToDepart)[0];
}

/**
 * Schedule sequential route
 * @param list
 * @return {number}
 */
function schedule(list) {
  // Original list
  const original = list
    .split(",")
    .map((shuttle) => (shuttle === "x" ? "x" : parseInt(shuttle)));

  // store shuttle drive time
  const shuttles = original.filter((shuttle) => shuttle !== "x");

  // Upper bounds of search space
  const maxIterations = shuttles.reduce((total, value) => total * value);

  // Start looking on this interval
  const maxInterval = shuttles.reduce((max, value) =>
    value > max ? value : max
  );

  // for we keep this manual for now
  const distanceRequire = shuttles.map((val) => original.indexOf(val));

  // For each timestamp we look for a recursive match
  let timestamp = 0,
    interval = maxInterval;
  while (timestamp <= maxIterations) {
    const matches = [];

    for (let i = 0; i < shuttles.length; i++) {
      // we update the schedule
      const shuttle = shuttles[i];

      // minutes distance required?
      const distanceRequired = distanceRequire[i];

      // Closest departure time
      const matchDeparture = (timestamp + distanceRequired) % shuttle === 0;

      // Do we have a match?
      if (matchDeparture) {
        matches.push(shuttle);
      } else {
        break;
      }
    }

    // We can assume the next match can only occur on an common multiplier of all previous matches
    if (matches.length > 1) {
      interval = lcm(...matches);
    }

    // Break out of loop
    if (matches.length === shuttles.length) {
      break;
    }

    // goto next interval and check again
    timestamp += interval;
  }
  return timestamp;
}

module.exports = {
  wait,
  schedule,
};
