"use strict";

/**
 * Check if character occurs between min and max times
 * @param passwords {Array<string>}
 * @return {Array<boolean>}
 */
function checkOccurrences(passwords) {
  return passwords.map((value) => {
    const [instructions, password] = value.split(": ");
    const [counts, char] = instructions.split(" ");
    const [min, max] = counts.split("-");
    const occurrences = Array.from(password).reduce((total, character) => {
      if (character === char) total += 1;
      return total;
    }, 0);
    return occurrences >= min && occurrences <= max;
  });
}

/**
 * Check character positions in a password
 * @param passwords {Array<string>}
 * @return {*}
 */
function checkCharPosition(passwords) {
  return passwords.map((value) => {
    const [instructions, password] = value.split(": ");
    const [counts, char] = instructions.split(" ");
    const [one, two] = counts.split("-").map((i) => parseInt(i) - 1);
    return (
      (password.charAt(one) === char && password.charAt(two) !== char) ||
      (password.charAt(one) !== char && password.charAt(two) === char)
    );
  });
}

module.exports = {
  checkOccurrences,
  checkCharPosition,
};
