"use strict";

/**
 * CharCodeAt() consts
 */
const FLOOR = 46;
const EMPTY_SEAT = 76;
const OCCUPIED_SEAT = 35;

/**
 * Find stable seating arrangement
 * @param map
 * @param type
 * @return {*}
 */
function stabilize(map, type = 1) {
  let previous = map,
    i = 0;
  while (true) {
    const next = type === 1 ? seating(previous) : seatingExpand(previous);
    if (identical(previous, next)) {
      return next;
    }
    previous = next;
    i++;
  }
}

/**
 * Count the number of occupied seats in a
 * @param a
 * @return {number}
 */
function occupied(a) {
  return a
    .flatMap((row) => row)
    .filter((seat) => seat.charCodeAt(0) === OCCUPIED_SEAT).length;
  /*
  let total = 0;
  for (let i = 0; i < a.length; i++) {
    for (let j = 0; j < a[i].length; j++) {
      if (a[i][j].charCodeAt(0) === OCCUPIED_SEAT) {
        total++;
      }
    }
  }
  return total;*/
}

/**
 * Compare if a is equal to b
 * @param a
 * @param b
 * @return {boolean}
 */
function identical(a, b) {
  for (let i = 0; i < a.length; i++) {
    for (let j = 0; j < a[i].length; j++) {
      if (a[i][j] !== b[i][j]) {
        return false;
      }
    }
  }
  return true;
}

/**
 * Calculate next seating iteration
 * @param map
 */
function seatingExpand(map) {
  const blank = map.map((i) => i.slice());
  for (let i = 0; i < map.length; i++) {
    for (let j = 0; j < map[i].length; j++) {
      // The seat
      const seat = map[i][j].charAt(0);

      // Surroundings
      const surrounding = new Map();

      // Find seat in top left diagonal until free or occupied seat
      // Expand until we have covered all the of map
      for (let k = 1; k < map.length; k++) {
        // multiply the offset direction
        const x = -1 * k,
          y = k;

        // Top Left
        const tl =
          i + x >= 0 && j + x >= 0
            ? map[i + x][j + x]
            : String.fromCharCode(FLOOR);

        // Top
        const t = i + x >= 0 ? map[i + x][j] : String.fromCharCode(FLOOR);

        // Top Right
        const tr =
          i + x >= 0 && j + y < map[i].length
            ? map[i + x][j + y]
            : String.fromCharCode(FLOOR);

        // Left
        const l = j + x >= 0 ? map[i][j + x] : String.fromCharCode(FLOOR);

        // Right
        const r =
          j + y < map[i].length ? map[i][j + y] : String.fromCharCode(FLOOR);

        // Bottom Left
        const bl =
          i + y < map.length && j + x >= 0
            ? map[i + y][j + x]
            : String.fromCharCode(FLOOR);

        // Bottom
        const b =
          i + y < map.length ? map[i + y][j] : String.fromCharCode(FLOOR);

        // Bottom Right
        const br =
          i + y < map.length && j + y < map[i].length
            ? map[i + y][j + y]
            : String.fromCharCode(FLOOR);

        // Setup points for iteration
        const points = { tl, t, tr, l, r, bl, b, br };

        // loop over points, check if we find and empty or occupied seat and store it as seat surroundings
        for (const key in points) {
          if (
            (points[key].charCodeAt(0) === EMPTY_SEAT ||
              points[key].charCodeAt(0) === OCCUPIED_SEAT) &&
            !surrounding.has(key)
          ) {
            surrounding.set(key, points[key]);
          }
        }

        // Break early
        if (surrounding.size === 8) {
          break;
        }
      }

      // map into array
      const arr = Array.from(surrounding).flatMap((i) => i[1]);

      // If a seat is empty (L) and there are no occupied seats adjacent to it, the seat becomes occupied.
      if (
        seat.charCodeAt(0) === EMPTY_SEAT &&
        arr.every((state) => state.charCodeAt(0) !== OCCUPIED_SEAT)
      ) {
        blank[i][j] = String.fromCharCode(OCCUPIED_SEAT);
      }
      // If a seat is occupied (#) and four or more seats adjacent to it are also occupied, the seat becomes empty.
      else if (
        seat.charCodeAt(0) === OCCUPIED_SEAT &&
        arr.filter((state) => state.charCodeAt(0) === OCCUPIED_SEAT).length >= 5
      ) {
        blank[i][j] = String.fromCharCode(EMPTY_SEAT);
      }
    }
  }
  return blank;
}

/**
 * Calculate next seating iteration
 * @param map
 */
function seating(map) {
  const blank = map.map((i) => i.slice());
  for (let i = 0; i < map.length; i++) {
    for (let j = 0; j < map[i].length; j++) {
      // The seat
      const seat = map[i][j].charAt(0);

      // Top Left
      const tl =
        i - 1 >= 0 && j - 1 >= 0
          ? map[i - 1][j - 1]
          : String.fromCharCode(FLOOR);

      // Top
      const t = i - 1 >= 0 ? map[i - 1][j] : String.fromCharCode(FLOOR);

      // Top Right
      const tr =
        i - 1 >= 0 && j + 1 < map[i].length
          ? map[i - 1][j + 1]
          : String.fromCharCode(FLOOR);

      // Left
      const l = j - 1 >= 0 ? map[i][j - 1] : String.fromCharCode(FLOOR);

      // Right
      const r =
        j + 1 < map[i].length ? map[i][j + 1] : String.fromCharCode(FLOOR);

      // Bottom Left
      const bl =
        i + 1 < map.length && j - 1 >= 0
          ? map[i + 1][j - 1]
          : String.fromCharCode(FLOOR);

      // Bottom
      const b = i + 1 < map.length ? map[i + 1][j] : String.fromCharCode(FLOOR);

      // Bottom Right
      const br =
        i + 1 < map.length && j + 1 < map[i].length
          ? map[i + 1][j + 1]
          : String.fromCharCode(FLOOR);

      //
      const surrounding = [tl, t, tr, l, r, bl, b, br];

      // If a seat is empty (L) and there are no occupied seats adjacent to it, the seat becomes occupied.
      if (
        seat.charCodeAt(0) === EMPTY_SEAT &&
        surrounding.every((state) => state.charCodeAt(0) !== OCCUPIED_SEAT)
      ) {
        blank[i][j] = String.fromCharCode(OCCUPIED_SEAT);
      }
      // If a seat is occupied (#) and four or more seats adjacent to it are also occupied, the seat becomes empty.
      else if (
        seat.charCodeAt(0) === OCCUPIED_SEAT &&
        surrounding.filter((state) => state.charCodeAt(0) === OCCUPIED_SEAT)
          .length >= 4
      ) {
        blank[i][j] = String.fromCharCode(EMPTY_SEAT);
      }
    }
  }
  return blank;
}

module.exports = {
  identical,
  occupied,
  seating,
  seatingExpand,
  stabilize,
};
