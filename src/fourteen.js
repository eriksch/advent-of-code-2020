"use strict";

const MASK = "mask";
const MASK_FLOATING = "X";

/**
 * Initialize docking computer
 * @param input
 * @return {*}
 */
function initialize(input) {
  const mem = [];
  let mask = 0;
  input.forEach((line) => {
    const [command, data] = line.split(" = ");
    if (command === MASK) {
      mask = data
        .split("")
        .map((i) => (i !== MASK_FLOATING ? parseInt(i) : MASK_FLOATING));
    } else {
      const addr = parseInt(command.match(/(\d+)/).shift());
      const bin = Number(parseInt(data)).toString(2);
      const value = Array.from(bin).map((i) => parseInt(i));
      value.splice(0, 0, ...new Array(36 - bin.length).fill(0));
      const result = value.map((val, index) =>
        mask[index] === MASK_FLOATING ? val : mask[index]
      );
      mem[addr] = result;
    }
  });
  return mem.reduce((total, value) => total + parseInt(value.join(""), 2), 0);
}

/**
 * Recursive replace of Floating bits to generate all addresses
 * @param addr
 * @return {[]}
 */
function replaceFloatingBits(addr) {
  const list = [];
  if (addr.some((a) => a === MASK_FLOATING)) {
    const index = addr.indexOf(MASK_FLOATING);
    const addrOne = addr.slice();
    addrOne.splice(index, 1, 1);
    list.push(...replaceFloatingBits(addrOne));

    const addrZero = addr.slice();
    addrZero.splice(index, 1, 0);
    list.push(...replaceFloatingBits(addrZero));
  } else {
    list.push(addr);
  }
  return list;
}

/**
 * Initialize docking computer with V2 decoder chip
 * @param input
 * @return {number}
 */
function initializeV2(input) {
  const mem = new Map();
  let mask = 0;
  for (let i = 0; i < input.length; i++) {
    const line = input[i];
    // Read command and data
    const [command, data] = line.split(" = ");

    // Handle new Mask
    if (command === MASK) {
      mask = data
        .split("")
        .map((i) => (i !== MASK_FLOATING ? parseInt(i) : MASK_FLOATING));
      // Handle new Data
    } else {
      // initial address
      const addr = parseInt(command.match(/(\d+)/).shift());
      const binAddr = Number(addr).toString(2);
      const addrValue = Array.from(binAddr).map((i) => parseInt(i));
      addrValue.splice(0, 0, ...new Array(36 - addrValue.length).fill(0));
      const addrResult = addrValue.map((val, index) =>
        mask[index] === 0 ? val : mask[index]
      );
      const addrs = replaceFloatingBits(addrResult);

      // value to write
      const bin = Number(parseInt(data)).toString(2);
      const value = Array.from(bin).map((i) => parseInt(i));
      value.splice(0, 0, ...new Array(36 - bin.length).fill(0));
      for (let j = 0; j < addrs.length; j++) {
        const addr = parseInt(addrs[j].join(""), 2);
        const val = parseInt(value.join(""), 2);
        mem.set(addr, val);
      }
    }
  }

  let total = 0;
  for (let value of mem.values()) {
    total += value;
  }
  return total;
}

module.exports = {
  initialize,
  initializeV2,
};
