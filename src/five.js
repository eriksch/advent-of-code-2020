"use strict";

/**
 * Decode boarding pass binary number to int
 * @param pass {string}
 * @return {number}
 */
function decodeBoardingPass(pass) {
  const bin = pass
    .replace(new RegExp("B", "g"), 1)
    .replace(new RegExp("F", "g"), 0)
    .replace(new RegExp("R", "g"), 1)
    .replace(new RegExp("L", "g"), 0);
  return parseInt(bin, 2);
}

module.exports = {
  decodeBoardingPass,
};
