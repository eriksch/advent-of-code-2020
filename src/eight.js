"use strict";

/**
 * Run bootloader code
 * @param bootloader {Array<string>}
 * @return {{acc: number, ended: boolean}}
 */
function run(bootloader) {
  let acc = 0,
    ip = 0,
    log = [ip];
  while (true) {
    const [operation, change] = bootloader[ip].split(" ");
    switch (operation) {
      case "nop":
        ++ip;
        break;
      case "acc":
        acc += parseInt(change, 10);
        ++ip;
        break;
      case "jmp":
        ip += parseInt(change, 10);
        break;
    }
    if (log.indexOf(ip) >= 0 || ip >= bootloader.length) {
      break;
    } else {
      log.push(ip);
    }
  }
  return {
    acc,
    ended: ip >= bootloader.length,
  };
}

/**
 * Attempt to fix a broken NOP or JMP instruction
 * @param bootloader {Array<string>}
 * @return {number}
 */
function fix(bootloader) {
  let result = 0;
  for (let i = 0; i < bootloader.length; i++) {
    const [operation, change] = bootloader[i].split(" ");
    const copy = [...bootloader];

    if (operation === "nop") {
      copy[i] = `jmp ${change}`;
    }

    if (operation === "jmp") {
      copy[i] = `nop ${change}`;
    }
    const { acc, ended } = run(copy);

    if (ended) {
      result = acc;
      break;
    }
  }
  return result;
}

module.exports = {
  run,
  fix,
};
