"use strict";

/**
 * Run last spoken game
 * @param input
 * @return {*}
 */
function lastSpoken(input, turn = 0) {
  const numbers = input.split(",").map((i) => parseInt(i));
  let i = 0,
    newNumber = 0,
    spoken = new Map(),
    spokenAt = new Map(),
    spokenBefore = new Map(),
    logAt = turn / 100;
  while (i <= turn) {
    // First round just print numbers
    if (spoken.size < numbers.length) {
      newNumber = numbers[i];
    } else {
      // The last number that was spoken
      const lastNumberSpoken = spoken.get(i - 1);

      // was it spoken for the first time?
      if (!spokenBefore.has(lastNumberSpoken)) {
        // Then the new number is 0
        newNumber = 0;

        // Make sure the number is added to the spoken before list
        spokenBefore.set(lastNumberSpoken, true);
      } else {
        // find out the difference between the last and before last times this number was spoken
        const [beforeLast, last] = spokenAt.get(lastNumberSpoken);
        newNumber = last - beforeLast;
      }
    }

    // add number to list of spoken numbers
    spoken.set(i, newNumber);

    // init with empty array
    if (!spokenAt.has(newNumber)) {
      spokenAt.set(newNumber, [0, i]);
    } else {
      const [last, beforeLast] = spokenAt.get(newNumber);
      spokenAt.set(newNumber, [beforeLast, i]);
    }

    // store for
    if (i > 0) {
      spokenBefore.set(spoken.get(i - 1), true);
    }

    // Cleanup
    if (i > numbers.length) {
      spoken.delete(i - numbers.length);
    }
    i++;
  }
  return newNumber;
}

module.exports = {
  lastSpoken,
};
