const memoize = require("memoizee");

/**
 * Calculate adapter chain
 * @param unsorted
 * @return {number}
 */
function chain(unsorted) {
  const adapters = unsorted.sort((a, b) => a - b);
  const maxJolts = adapters.slice().pop();
  let jolts = 0,
    diffOne = 0,
    diffThree = 1;
  while (jolts < maxJolts) {
    const adapter = adapters.shift();
    const diff = Math.abs(adapter - jolts);
    if (diff === 1) ++diffOne;
    else if (diff === 3) ++diffThree;
    jolts += diff;
  }
  return diffOne * diffThree;
}

/**
 * Finds possible set of adapters
 * @param list
 * @param maxJolts
 * @param set
 * @param index
 * @return {number}
 */
let findSets = (list, maxJolts, set, index) => {
  let sets = 0;
  let jolts = set;
  while (jolts < maxJolts) {
    const adapter = list[index];
    if (adapter - jolts <= 3) {
      if (list[index + 1] - jolts <= 3) {
        sets += findSets(list, maxJolts, jolts, index + 1);
      }
    }
    jolts = adapter;
    index++;
  }
  return ++sets;
};

// Memoize recursive function
findSets = memoize(findSets);

/**
 *
 * @param unsorted
 * @return {number}
 */
function combinations(unsorted) {
  const adapters = unsorted.sort((a, b) => a - b);
  const maxJolts = adapters.slice().pop();
  let index = 0,
    set = 0;
  return findSets(adapters, maxJolts, set, index);
}

module.exports = {
  chain,
  combinations,
};
