"use strict";
const fs = require("fs");

module.exports = {
  readFileIntoArray: (path, separator = "\n", trim = true) => {
    const list = fs.readFileSync(path).toString().split(separator);
    return trim ? list.slice(0, list.length - 1) : list;
  },
};
