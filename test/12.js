const test = require("ava");
const { sailDirection, sailWaypoint } = require("../src/twelve");
const { readFileIntoArray } = require("../src/util/fs");

test("Day 12: Rain Risk - Test case A", (t) => {
  const instructions = readFileIntoArray("test/twelve/test.txt");
  const { position, manhattan } = sailDirection(instructions);
  t.log("position", position, "manhattan: ", manhattan);
  t.assert(manhattan === 25);
});

test("Day 12: Rain Risk - Solution A", (t) => {
  const instructions = readFileIntoArray("input/twelve.txt");
  const { position, manhattan } = sailDirection(instructions);
  t.log("position", position, "manhattan: ", manhattan);
  t.pass();
});

test("Day 12: Rain Risk - Test case B", (t) => {
  const instructions = readFileIntoArray("test/twelve/test.txt");
  const { position, manhattan } = sailWaypoint(instructions);
  t.log("position", position, "manhattan: ", manhattan);
  t.assert(manhattan === 286);
});

test("Day 12: Rain Risk - Solution B", (t) => {
  const instructions = readFileIntoArray("input/twelve.txt");
  const { position, manhattan } = sailWaypoint(instructions);
  t.log("position", position, "manhattan: ", manhattan);
  t.pass();
});
