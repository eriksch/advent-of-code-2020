const test = require("ava");
const { readFileIntoArray } = require("../src/util/fs");
const { decodeBoardingPass } = require("../src/five");

test("Day 5: Binary Boarding - Test case A", (t) => {
  t.assert(567 === decodeBoardingPass("BFFFBBFRRR"));
  t.assert(119 === decodeBoardingPass("FFFBBBFRRR"));
  t.assert(820 === decodeBoardingPass("BBFFBBFRLL"));
});

test("Day 5: Binary Boarding - Solution A", (t) => {
  const passes = readFileIntoArray("input/five.txt");
  const [seat] = passes
    .map((pass) => decodeBoardingPass(pass))
    .sort((a, b) => b - a);
  t.log("Answer:", seat);
  t.pass();
});

test("Day 5: Binary Boarding - Solution B", (t) => {
  const passes = readFileIntoArray("input/five.txt");
  const sorted = passes
    .map((pass) => decodeBoardingPass(pass))
    .sort((a, b) => a - b);
  let previous = sorted[0] - 1,
    answer = -1;
  for (let seat of sorted) {
    if (seat - 1 !== previous) {
      answer = seat - 1;
      break;
    }
    previous = seat;
  }
  t.log("Answer:", answer);
  t.pass();
});
