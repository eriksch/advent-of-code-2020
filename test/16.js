const test = require("ava");
const { readFileIntoArray } = require("../src/util/fs");
/*

test('Day 16: Ticket Translation - Test case A', t => {
  const lines = readFileIntoArray('test/sixteen/test.txt');

  function * inputParser(lines) {
    let block = [];
    for (let i = 0; i < lines.length; i++) {
      if (lines[i].length === 0) {
        yield block;
        block = [];
      } else {
        block.push(lines[i]);
      }
    }
    yield block;
  }

  function translateTicket( lines ) {
    const parser = inputParser(lines);
    const fields = parser.next().value;
    const myTicket = parser.next().value.slice(1).map(t => t.split(',').map(i => parseInt(i)));
    const nearbyTickets = parser.next().value.slice(1).map(t => t.split(',').map(i => parseInt(i)));
    const ranges = fields.flatMap(field => {
      const [name, rangeBlock] = field.split(':');
      const blocks = rangeBlock.split(' or ');
      return blocks.map(block => block.split('-').map(i => parseInt(i)));
    });

    console.log('ranges', ranges);
    console.log('nearbyTickets', nearbyTickets);

    const errors = nearbyTickets.flatMap(ticket =>
      ticket.flatMap(value =>
        ranges.some(([start, end]) => value >= start && value <= end) ? [] : [value]
      )
    );

    const errorRate = errors.reduce((total, value) => total + value);

    console.log('errors', errors);
    console.log('errorRate', errorRate);


    return errorRate;
  }

  const errors = translateTicket(lines);
  t.assert(errors === 71);
});
*/

/*

test('Day 16: Ticket Translation - Solution A', t => {
  const lines = readFileIntoArray('input/sixteen.txt');

  /!**
   * Read input into blocks
   * @param lines
   * @return {Generator<[], void, *>}
   *!/
  function * inputParser(lines) {
    let block = [];
    for (let i = 0; i < lines.length; i++) {
      if (lines[i].length === 0) {
        yield block;
        block = [];
      } else {
        block.push(lines[i]);
      }
    }
    yield block;
  }

  /!**
   * Translate input into fields and tickets
   * @param lines
   * @return {{ranges: *[], nearbyTickets: number[][], myTicket: number[][]}}
   *!/
  function translateTicket( lines ) {
    const parser = inputParser(lines);
    const fields = parser.next().value;
    const myTicket = parser.next().value.slice(1).map(t => t.split(',').map(i => parseInt(i)));
    const nearbyTickets = parser.next().value.slice(1).map(t => t.split(',').map(i => parseInt(i)));
    const ranges = fields.flatMap(field => {
      const [name, rangeBlock] = field.split(':');
      const blocks = rangeBlock.split(' or ');
      return blocks.map(block => block.split('-').map(i => parseInt(i)));
    });
    return {
      myTicket,
      nearbyTickets,
      ranges
    }
  }

  /!**
   * Return error rate for tickets
   * @param nearbyTickets
   * @param ranges
   * @return Number
   *!/
  function calculateErrorRate(nearbyTickets, ranges) {
    return nearbyTickets.flatMap(ticket =>
      ticket.flatMap(value =>
        ranges.some(([start, end]) => value >= start && value <= end) ? [] : [value]
      )
    ).reduce((total, value) => total + value);
  }
  const {nearbyTickets, ranges} = translateTicket(lines);
  const errorRate = calculateErrorRate(nearbyTickets, ranges);
  t.pass();
});

*/

/**
 * Read input into blocks
 * @param lines
 * @return {Generator<[], void, *>}
 */
function* inputParser(lines) {
  let block = [];
  for (let i = 0; i < lines.length; i++) {
    if (lines[i].length === 0) {
      yield block;
      block = [];
    } else {
      block.push(lines[i]);
    }
  }
  yield block;
}

/**
 * Translate input into fields and tickets
 * @param lines
 * @return {{ranges: *[], nearbyTickets: number[][], myTicket: number[][]}}
 */
function translateTicket(lines) {
  const parser = inputParser(lines);
  const fields = parser
    .next()
    .value.map((f) => f.split(":"))
    .map(([name, rangeBlock]) => [
      name,
      rangeBlock
        .split(" or ")
        .map((block) => block.split("-").map((i) => parseInt(i))),
    ]);
  const myTicket = parser
    .next()
    .value.slice(1)
    .map((t) => t.split(",").map((i) => parseInt(i))).pop();
  const nearbyTickets = parser
    .next()
    .value.slice(1)
    .map((t) => t.split(",").map((i) => parseInt(i)));
  const ranges = fields.flatMap(([name, blocks]) => blocks);
  return {
    fields,
    myTicket,
    nearbyTickets,
    ranges,
  };
}

/**
 * Only return valid tickets
 * @param nearbyTickets
 * @param ranges
 * @return {*[]}
 */
function validTickets(nearbyTickets, ranges) {
  return nearbyTickets.filter((ticket) =>
    ticket.every((value) =>
      ranges.some(([start, end]) => value >= start && value <= end)
    )
  );
}

/**
 * Return error rate for tickets
 * @param nearbyTickets
 * @param ranges
 * @return Number
 */
function calculateErrorRate(nearbyTickets, ranges) {
  return nearbyTickets
    .flatMap((ticket) =>
      ticket.flatMap((value) =>
        ranges.some(([start, end]) => value >= start && value <= end)
          ? []
          : [value]
      )
    )
    .reduce((total, value) => total + value);
}

/**
 *
 */
test("Day 16: Ticket Translation - Test case B", (t) => {
  const lines = readFileIntoArray("input/sixteen.txt");
  const { fields, myTicket, nearbyTickets, ranges } = translateTicket(lines);
  const valid = validTickets(nearbyTickets, ranges);

  let candidates = new Map();
  for (let i = 0; i < myTicket.length; i++) {
    candidates.set(i, new Set());
    fields.forEach(([f, v]) => {
      if (valid.every(t => v.some(r => t[i] >= r[0] && t[i] <= r[1]))) {
        candidates.get(i).add(f);
      }
    });
  }

  const names = new Map();
  while (candidates.size) {
    const [index] = Array.from(candidates.entries()).find(([index, candidate]) => candidate.size === 1);
    candidates.get(index).forEach(name => {
      names.set(index, name);
    });
    candidates.delete(index);
    candidates.forEach(candidate => candidate.delete(names.get(index)));
  }
  console.log('names', names);

  const total = [];
  myTicket.forEach(( value, index) => {
    if (names.get(index).indexOf("departure") === 0) {
      total.push(value);
    }
  });

  console.log(total, total.reduce((sum, value) => sum *= value));

/*
  return math.prod(n for i, n in enumerate(ticket)
    if names[i].startswith('departure'))
  */
  t.log('Answer: ', total);
  t.pass();
});
