const test = require("ava");
const { readFileIntoArray } = require("../src/util/fs");
const { initialize, initializeV2 } = require("../src/fourteen");

test("Day 14: Docking Data - Test case A", (t) => {
  const input = readFileIntoArray("test/fourteen/testA.txt");
  t.assert(initialize(input) === 165);
});

test("Day 14: Docking Data - Solution A", (t) => {
  const input = readFileIntoArray("input/fourteen.txt");
  t.log("Answer:", initialize(input));
  t.pass();
});

test("Day 14: Docking Data - Test case B", (t) => {
  const input = readFileIntoArray("test/fourteen/testB.txt");
  t.assert(initializeV2(input) === 208);
});

test("Day 14: Docking Data - Solution B", (t) => {
  const input = readFileIntoArray("input/fourteen.txt");
  t.log("Answer:", initializeV2(input));
  t.pass();
});
