const test = require("ava");
const { run, fix } = require("../src/eight");
const { readFileIntoArray } = require("../src/util/fs");

test("Day 8: Handheld Halting - Test case A", (t) => {
  const bootloader = readFileIntoArray("test/eight/test.txt");
  const { acc } = run(bootloader);
  t.assert(acc === 5);
});

test("Day 8: Handheld Halting - Solution A", (t) => {
  const bootloader = readFileIntoArray("input/eight.txt");
  const { acc } = run(bootloader);
  t.log("Answer:", acc);
  t.pass();
});

test("Day 8: Handheld Halting - Test case B", (t) => {
  const bootloader = readFileIntoArray("test/eight/test.txt");
  t.assert(fix(bootloader) === 8);
});

test("Day 8: Handheld Halting - Solution B", (t) => {
  const bootloader = readFileIntoArray("input/eight.txt");
  t.log("Answer:", fix(bootloader));
  t.pass();
});
