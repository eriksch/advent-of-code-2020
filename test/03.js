const test = require("ava");
const { lookupTrees } = require("../src/three");
const { readFileIntoArray } = require("../src/util/fs");

test("Day 3: Toboggan Trajectory - Solution A", (t) => {
  const forrest = readFileIntoArray("input/three.txt");
  const { trees } = lookupTrees(forrest, 3, 1);
  t.log("Answer:", trees);
  t.pass("done");
});

test("Day 3: Toboggan Trajectory - Solution B", (t) => {
  const forrest = readFileIntoArray("input/three.txt");
  const paths = [
    [1, 1],
    [3, 1],
    [5, 1],
    [7, 1],
    [1, 2],
  ];
  const solutions = paths.map(([right, down]) =>
    lookupTrees(forrest, right, down)
  );
  t.log(
    "Answer:",
    solutions
      .map((solution) => solution.trees)
      .reduce((total, trees) => total * trees)
  );
  t.pass("done");
});
