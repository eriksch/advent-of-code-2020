const test = require("ava");
const { wait, schedule } = require("../src/thirteen");
const { readFileIntoArray } = require("../src/util/fs");

test("Day 13: Shuttle Search - Test case A", (t) => {
  const [timestamp, list] = readFileIntoArray("test/thirteen/test.txt");
  t.assert(wait(timestamp, list) === 295);
});

test("Day 13: Shuttle Search - Solution A", (t) => {
  const [timestamp, list] = readFileIntoArray("input/thirteen.txt");
  t.log("Answer:", wait(timestamp, list));
  t.pass();
});

test("Day 13: Shuttle Search - Test case  B", (t) => {
  const [bus, list] = readFileIntoArray("test/thirteen/test.txt");
  t.assert(schedule(list) === 1068781);
  t.pass();
});

test("Day 13: Shuttle Search - Solution  B", (t) => {
  const [bus, list] = readFileIntoArray("input/thirteen.txt");
  t.log("Answer:", schedule(list));
  t.pass();
});
