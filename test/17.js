const test = require('ava');
const {readFileIntoArray} = require("../src/util/fs");
const cloneDeep = require('clone-deep');

test('Day 17: Conway Cubes - Test case A', t => {
  const layers = readFileIntoArray('test/seventeen/test.txt').map(line => line.split(''));

  const ACTIVE = '#';
  const INACTIVE = '.';

  /**
   * set cell
   * @param space
   * @param size
   * @param x
   * @param y
   * @param z
   * @param value
   */
  function setCell(space, size, x, y, z, value) {
    if (!space[x + size]) space[x + size] = [];
    if (!space[x + size][y + size]) space[x + size][y + size] = [];
    space[x + size][y + size][z + size] = value;
  }

  /**
   * Get cell
   * @param space
   * @param size
   * @param x
   * @param y
   * @param z
   * @return {*}
   */
  function getCell(space, size, x, y, z) {
    if (space[x + size] !== undefined && space[x + size][y + size] !== undefined && space[x + size][y + size][z + size] !== undefined) {
      return space[x + size][y + size][z + size];
    } else {
      return INACTIVE;
    }
  }

  function getCells(space, size) {
    const cells = [];
    for (let i = -size; i <= size; i++) {
      for (let j = -size; j <= size; j++) {
        for (let k = -size; k <= size; k++) {
          cells.push(getCell(space, size, i, j, k))
        }
      }
    }
    return cells;
  }

  /**
   * Get neighbours for cell
   * @param space
   * @param x
   * @param y
   * @param z
   */
  function getNeighbours(space, size, x, y, z) {
    const offsets = [-1, 0, 1];
    const neighbours = [];
    for (let i = 0; i < offsets.length; i++) {
      for (let j = 0; j < offsets.length; j++) {
        for (let k = 0; k < offsets.length; k++) {
          if (!(offsets[i] === 0 && offsets[j] === 0 && offsets[k] === 0)) {
            neighbours.push(getCell(space, size,x + offsets[i], y + offsets[j], z + offsets[k]));
          }
        }
      }
    }
    return {
      neighbours,
      active: neighbours.filter(n => n === ACTIVE).length,
      inactive: neighbours.filter(n => n === INACTIVE).length
    };
  }

  /**
   *
   * @param space
   * @param size
   */
  function printSpace(space, size) {
    const layers = [];
    for (let i = -size; i <= size; i++) {
      let zLine = "";
      for (let j = -size; j <= size; j++) {
        for (let k = -size; k <= size; k++) {
          zLine += getCell(space, size, j, k, i);
        }
        zLine += " " + j + "\n";
      }
      layers.push(zLine);
    }
    layers.forEach((layer, index) => {
      if (layer.indexOf(ACTIVE) !== -1) {
        console.log('layer:', index - size);
        console.log(layer);
      }
    })
  }

  /**
   * Solve
   * @param layers
   */
  function solveSpace(layers) {
    console.log('layers', layers);
    // Size - Width x Height
    const size = 10;
    const offset = Math.floor(layers.length / 2);

    // Space
    const space = [];

    // Z Layer
    const z = 0;

    // Setup space
    for (let i = 0; i < layers.length; i++) {
      for (let j = 0; j < layers.length; j++) {
        // offset for map size size
        const x = i - offset;
        const y = j - offset;
        // Set space
        setCell(space, size, x, y, z, layers[i][j]);
      }
    }

    console.log('Sequence:', 0);
    printSpace(space, size);

    let neighbours = null, sequence = 1, nextSpace = cloneDeep(space), previousSpace = cloneDeep(space);

    // Neighbours
    neighbours = getNeighbours(space, size, 0, 0,0);
    console.log('neighbours', neighbours.neighbours.join(''));

    // Neighbours
    neighbours = getNeighbours(previousSpace, size, 0, 0,0);
    console.log('neighbours', neighbours.neighbours.join(''));

    return;


    while(sequence < 2) {
      for (let i = -size; i <= size; i++) {
        for (let j = -size; j <= size; j++) {
          for (let k = -size; k <= size; k++) {
            // Cell
            const cell = getCell(previousSpace, size, i, j, k);

            // Neighbours
            const neighbours = getNeighbours(previousSpace, size, i, j, k);

            console.log('neighbours', neighbours);

            return;

            // Toggle cells
            if (cell === ACTIVE && (neighbours.active === 2 || neighbours.active === 3)) {
              setCell(nextSpace, size, i, j, k, ACTIVE);
            } else {
              setCell(nextSpace, size, i, j, k, INACTIVE);
            }

            // Activate inactive cells
            if (cell === INACTIVE && neighbours.active === 3) {
              setCell(nextSpace, size, i, j, k, ACTIVE);
            }
          }
        }
      }
      //console.log(sequence, nextSpace, getCells(nextSpace, size).filter(i => i === ACTIVE).length);
      if (sequence === 1) {
        //console.log('Sequence ', sequence, 'active', getCells(nextSpace, size).filter(i => i === ACTIVE).length);
        console.log('previous:', sequence);
        printSpace(previousSpace, size);
        console.log('next:', sequence);
        printSpace(nextSpace, size);
      }

      nextSpace = cloneDeep(previousSpace);
      previousSpace = nextSpace;

      sequence++;
    }


  }

  solveSpace(layers);

  t.pass();
});


