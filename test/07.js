const test = require("ava");
const {
  buildRuleSetA,
  buildRuleSetB,
  canContainBag,
  countBags,
} = require("../src/seven");
const { readFileIntoArray } = require("../src/util/fs");

test("Day 7: Handy Haversacks - Test case A", (t) => {
  const lines = readFileIntoArray("test/seven/testA.txt");
  const rules = buildRuleSetA(lines);
  const fit = Object.keys(rules).reduce(
    (total, bag) => (canContainBag(rules, bag, "shiny gold") ? ++total : total),
    0
  );
  t.assert(fit === 4);
});

test("Day 7: Handy Haversacks - Solution A", (t) => {
  const lines = readFileIntoArray("input/seven.txt");
  const rules = buildRuleSetA(lines);
  const fit = Object.keys(rules).reduce(
    (total, bag) => (canContainBag(rules, bag, "shiny gold") ? ++total : total),
    0
  );
  t.log("Answer:", fit);
  t.pass();
});

test("Day 7: Handy Haversacks - Test case B", (t) => {
  const lines = readFileIntoArray("test/seven/testB.txt");
  const rules = buildRuleSetB(lines);
  const total = countBags(rules, "shiny gold");
  t.assert(total === 126);
});

test("Day 7: Handy Haversacks - Solution B", (t) => {
  const lines = readFileIntoArray("input/seven.txt");
  const rules = buildRuleSetB(lines);
  const total = countBags(rules, "shiny gold");
  t.log("Answer:", total);
  t.pass();
});
