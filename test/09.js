const test = require("ava");
const { decode, attack } = require("../src/nine");
const { readFileIntoArray } = require("../src/util/fs");

test("Day 9: Encoding Error - Test case A", (t) => {
  const encrypted = readFileIntoArray("test/nine/test.txt").map((i) =>
    parseInt(i)
  );
  const vector = decode(encrypted, 5);
  t.assert(vector === 127);
});

test("Day 9: Encoding Error - Solution A", (t) => {
  const encrypted = readFileIntoArray("input/nine.txt").map((i) => parseInt(i));
  const vector = decode(encrypted, 25);
  t.log("Answer:", vector);
  t.pass();
});

test("Day 9: Encoding Error - Test case B", (t) => {
  const encrypted = readFileIntoArray("test/nine/test.txt").map((i) =>
    parseInt(i)
  );
  const vector = decode(encrypted, 5);
  const { low, high } = attack(encrypted, vector);
  t.assert(vector === 127 && low + high === 62);
});

test("Day 9: Encoding Error - Solution B", (t) => {
  const encrypted = readFileIntoArray("input/nine.txt").map((i) => parseInt(i));
  const vector = decode(encrypted, 25);
  const { low, high } = attack(encrypted, vector);
  t.log("vector:", vector);
  t.log("low:", low, "high:", high);
  t.log("Answer:", low + high);
  t.pass();
});
