const test = require("ava");
const { sumTwoToNeeded, sumThreeToNeeded } = require("../src/one");
const { readFileIntoArray } = require("../src/util/fs");

const NEEDED_TOTAL = 2020;

test("Day 1: Report Repair - Solution A", (t) => {
  const amounts = readFileIntoArray("input/one.txt").map((value) =>
    parseInt(value)
  );
  t.log("Answer: ", sumTwoToNeeded(amounts, NEEDED_TOTAL));
  t.pass();
});

test("Day 1: Report Repair - Solution B", (t) => {
  const amounts = readFileIntoArray("input/one.txt").map((value) =>
    parseInt(value)
  );
  t.log("Answer: ", sumThreeToNeeded(amounts, NEEDED_TOTAL));
  t.pass();
});
