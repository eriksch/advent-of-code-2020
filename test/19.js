const test = require("ava");
const { readFileIntoArray } = require("../src/util/fs");

function readRules(instructions) {
  const ruleMap = new Map();
  instructions.forEach((instruction) => {
    const [index, sets] = instruction.split(": ");
    ruleMap.set(
      index,
      sets.split("|").map((set) => {
        return set.trim().split(" ").map(s => {
          return s.indexOf('"') !== -1 ? s.substring(1,2) : s
        });
      })
    );
  });
  ruleMap.set('4', 'a');
  ruleMap.set('5', 'b');
  console.log(ruleMap);


  // function readRule(str, rule) {
  //   console.log('readRule', str, 'r', rule);
  //   if (rule === undefined) return;
  //   if (rule instanceof Array) {
  //     rule.forEach(r => {
  //       if (r instanceof Array) {
  //         if (r.length === 1) {
  //           if (r === 'a' || r === 'b') {
  //             str += r;
  //           } else {
  //             return readRule(str, ruleMap.get(r));
  //           }
  //         } else {
  //           return readRule(str, r);
  //         }
  //       } else {
  //         console.log('r', r, ruleMap.get(r));
  //         if (r === 'a' || r === 'b') {
  //           str += r;
  //         } else {
  //           return readRule(str, ruleMap.get(r));
  //         }
  //       }
  //     });
  //   } else {
  //     if (rule === 'a' || rule === 'b') {
  //       str += rule;
  //     } else {
  //       return readRule(str, ruleMap.get(rule));
  //     }
  //   }
  // }

  function readRule(str, ruleSet) {
    const list = [];

    if (ruleSet instanceof Array) {
      ruleSet.forEach(set => {
        console.log('resolve set: ', set);
        list.push(
          resolve(set)
        )
      });
    }
 /*   console.log('readRule', 'str', str, 'set', ruleSet, 'list', list);
    return ruleSet.map(set => {
      str = "";
      if (set instanceof Array) {
        set.forEach(s => {
          console.log('ss', s);
          str += (s === 'a' || s === 'b') ? s : readRule("", ruleMap.get(s));
        })
        console.log('set to str', str);
      }
      return str;
    });*/

    return list;
  }

  function resolve(rule) {
    const nextRule = ruleMap.get(rule);
    if (nextRule === 'a' || nextRule === 'b') {
      return nextRule;
    } else {
      return readRule(nextRule, nextRule);
    }
  }


  const ruleTree = readRule("", ruleMap.get('0'));


  const result = ['aaaabb', 'aaabab', 'abbabb', 'abbbab', 'aabaab', 'aabbbb', 'abaaab', 'ababbb'];

  console.log('list:', ruleTree);

  /*function buildStrings(str, rules) {
    let strings = [];
    rules.forEach(rule => {
      console.log('rule', rule);
      if (rule.length === 1 && rule[0].length === 1) {
        str += rule[0][0];
      } else {
        strings = [...strings, ...rule.map(r => buildStrings(str, r))];
      }
    });
    strings.push(str);
    return strings;
  }*/
/*

  console.log(buildStrings("", ruleTree));

  console.log('joined', ruleTree);
  let ruleStrings = [];
  ruleTree.forEach(rule => {
    console.log(rule[0][0]);
    let ruleString = "";
    if (rule.length === 1 && rule[0].length === 1) {
      ruleString = rule[0][0];
    }


    ruleStrings.push(ruleString);
  })
  console.log(ruleStrings);


*/



  //aaaabb, aaabab, abbabb, abbbab, aabaab, aabbbb, abaaab, or ababbb.

}

test("Day 19: Monster Messages", (t) => {
  const [instructions, code] = readFileIntoArray(
    "test/nineteen/test.txt",
    "\n\n",
    false
  ).map((lines) => lines.split("\n"));
  readRules(instructions);
  t.pass();
});
