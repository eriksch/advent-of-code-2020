const test = require("ava");
const { readFileIntoArray } = require("../src/util/fs");
const { checkOccurrences, checkCharPosition } = require("../src/two");

test("Day 2: Password Philosophy - Solution A", (t) => {
  const passwords = readFileIntoArray("input/two.txt");
  t.log(
    "Answer:",
    checkOccurrences(passwords).reduce(
      (total, valid) => (valid ? ++total : total),
      0
    )
  );
  t.pass();
});

test("Day 2: Password Philosophy - Solution B", (t) => {
  const passwords = readFileIntoArray("input/two.txt");
  t.log(
    "Answer:",
    checkCharPosition(passwords).reduce(
      (total, valid) => (valid ? ++total : total),
      0
    )
  );
  t.pass();
});
