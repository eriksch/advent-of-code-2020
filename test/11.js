const test = require("ava");
const { readFileIntoArray } = require("../src/util/fs");
const {
  seating,
  identical,
  occupied,
  stabilize,
  seatingExpand,
} = require("../src/eleven");

test("Day 11: Seating System - Test case A", (t) => {
  const source = readFileIntoArray("test/eleven/test.txt").map((i) =>
    i.split("")
  );
  const one = readFileIntoArray("test/eleven/1.txt").map((i) => i.split(""));
  const two = readFileIntoArray("test/eleven/2.txt").map((i) => i.split(""));
  const three = readFileIntoArray("test/eleven/3.txt").map((i) => i.split(""));
  const four = readFileIntoArray("test/eleven/4.txt").map((i) => i.split(""));
  const five = readFileIntoArray("test/eleven/5.txt").map((i) => i.split(""));

  const firstIteration = seating(source);
  t.assert(identical(one, firstIteration));

  const secondIteration = seating(firstIteration);
  t.assert(identical(two, secondIteration));

  const thirdIteration = seating(secondIteration);
  t.assert(identical(three, thirdIteration));

  const fourthIteration = seating(thirdIteration);
  t.assert(identical(four, fourthIteration));

  const fifthIteration = seating(fourthIteration);
  t.assert(identical(five, fifthIteration));

  t.assert(occupied(fifthIteration) === 37);
});

test("Day 11: Seating System - Solution A", (t) => {
  const source = readFileIntoArray("input/eleven.txt").map((i) => i.split(""));
  t.log("Answer:", occupied(stabilize(source)));
  t.pass();
});

test("Day 11: Seating System - Test case B", (t) => {
  const source = readFileIntoArray("test/eleven/test.txt").map((i) =>
    i.split("")
  );
  const one = readFileIntoArray("test/eleven/1.txt").map((i) => i.split(""));
  const two = readFileIntoArray("test/eleven/2e.txt").map((i) => i.split(""));
  const three = readFileIntoArray("test/eleven/3e.txt").map((i) => i.split(""));
  const four = readFileIntoArray("test/eleven/4e.txt").map((i) => i.split(""));
  const five = readFileIntoArray("test/eleven/5e.txt").map((i) => i.split(""));

  const firstIteration = seatingExpand(source);
  t.assert(identical(one, firstIteration));

  const secondIteration = seatingExpand(firstIteration);
  t.assert(identical(two, secondIteration));

  const thirdIteration = seatingExpand(secondIteration);
  t.assert(identical(three, thirdIteration));

  const fourthIteration = seatingExpand(thirdIteration);
  t.assert(identical(four, fourthIteration));

  const fifthIteration = seatingExpand(fourthIteration);
  t.assert(identical(five, fifthIteration));

  t.assert(occupied(fifthIteration) === 31);
});

test("Day 11: Seating System - Solution B", (t) => {
  const source = readFileIntoArray("input/eleven.txt").map((i) => i.split(""));
  t.log("Answer:", occupied(stabilize(source, 2)));
  t.pass();
});
